import { Routes } from '@angular/router';
import {PostComponent} from './post/post.component';
import {PostListViewComponent} from './post-list-view/post-list-view.component';
import {FormComponent} from './form/form.component';
import {LoginComponent} from './login/login.component';
import {Error404Component} from './error-404/error-404.component';
import {AuthGuard} from './auth.guard';



export const appRoutes: Routes = [
  { path: 'dogs', component: PostListViewComponent},
  { path: 'dogs/:id', component: PostComponent},
  { path: 'add', component: FormComponent, canActivate : [AuthGuard]},
  {path : '404', component: Error404Component},
  { path: 'login', component: LoginComponent},
  { path: '', component: PostListViewComponent},
  /***Must stay at the end***/
  {path: '**', redirectTo: '404' }
]
