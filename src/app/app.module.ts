
/***Modules***/
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

/***Services***/

import {PostService} from './services/post.service';
import {AuthService} from './services/auth.service';

/***Routes**/
import {appRoutes} from './routes';
import {RouterModule} from '@angular/router';
import {AuthGuard} from './auth.guard';

/***Component***/
import { AppComponent } from './app.component';
import { PostListComponent } from './post-list/post-list.component';
import { FormComponent } from './form/form.component';
import { LoginComponent } from './login/login.component';
import { PostComponent } from './post/post.component';
import { PostListViewComponent } from './post-list-view/post-list-view.component';
import { NavbarComponent } from './navbar/navbar.component';
import {FormsModule} from '@angular/forms';
import {Error404Component} from './error-404/error-404.component';
import {HttpClient} from '@angular/common/http';



@NgModule({
  declarations: [
    AppComponent,
    PostListComponent,
    FormComponent,
    LoginComponent,
    PostComponent,
    PostListViewComponent,
    Error404Component,
    NavbarComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    HttpClientModule
  ],
  providers: [
    PostService,
    AuthService,
    AuthGuard,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
