import { Component, OnInit } from '@angular/core';
import {PostService} from '../services/post.service';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  constructor(private postService: PostService, private route: Router) { }

  ngOnInit() {
  }

  /*Ad a Post from the form*/
  onSubmit(form: NgForm) {
    let post = form.value
    post.likes = 0
    post.date = new Date()
    this.postService.addPost(post)
    this.route.navigate(['/dogs'])
  }

}
