import { Component, OnInit } from '@angular/core';
import {AuthService} from '../services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  title = 'Dogs rate';
  isLog = this.authService.isAuthenticated

  constructor(private authService: AuthService) { }

  ngOnInit() {
  }

}
