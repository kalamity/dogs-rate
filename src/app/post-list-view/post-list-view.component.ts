import {Component, OnInit} from '@angular/core';
import {PostService} from '../services/post.service';

@Component({
  selector: 'app-post-list-view',
  templateUrl: './post-list-view.component.html',
  styleUrls: ['./post-list-view.component.css']
})
export class PostListViewComponent implements OnInit {
  posts: any[];

  constructor(private postService: PostService) { }

  ngOnInit() {
    this.postService.postsSubject.subscribe(
      (posts) => this.posts = posts
    )
    this.postService.getPosts()
  }


}
