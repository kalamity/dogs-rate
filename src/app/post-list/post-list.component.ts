import { Component, Input, OnInit } from '@angular/core';
import {PostService} from '../services/post.service';
import {AuthService} from '../services/auth.service';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {
  @Input() private name: string;
  @Input() private description: string;
  @Input() private image: string;
  @Input() private date: Date;
  @Input() private id: number;
  @Input ()private likes: number;

  public isLog = this.authService.isAuthenticated


  constructor(private authService : AuthService, private postService: PostService) { }

  ngOnInit() {
  }

  like() {
    this.postService.like(this.id);
  }

  delete(){
    this.postService.deletePost(this)
  }
}
