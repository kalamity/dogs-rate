import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {

  public isAuthenticated: boolean = false;

  constructor() { }

  login() {
    const promise = new Promise((resolve, reject) => {
      setTimeout(() => {
        this.isAuthenticated = true;
        resolve();
      }, 2000);
    })

    return promise;
  }

  logout() {
    this.isAuthenticated = false;
  }

}
