import {Injectable} from '@angular/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class PostService {
  /***Datas***/
  posts = [];
  public postsSubject: Subject<any>;
  private readonly api: string = `https://dogrates-84237.firebaseio.com`;

  constructor(private httpClient: HttpClient) {

    this.postsSubject = new Subject<any>();
  }

  emitPosts() {
    this.postsSubject.next(this.posts); // mise a jour de l'annonce dans tout les composant
  }

  /***Get the dogs : get posts ***/
  getPosts() {
    this.httpClient.get<any[]>(`${this.api}/posts.json`)
      .subscribe(
        data => {
          let posts = [];
          let p = new Map();
          for (let [id, post] of Object.entries(data)) {
            post.id = id;
            post.date = Date.parse(post.date);
            posts.push(post);
          }
          this.posts = posts;
          this.emitPosts();
        },
        error => {
          console.error(error);
        },
        () => {
          console.info('GET ads complete');
        }
      );
  }

  /***Add a Dog :  Ad a post***/
  public addPost(post) {
    return this.httpClient.post<{ name }>(`${this.api}/posts.json`, post).subscribe(
      data => {
        post.id = data.name;
        this.posts.push(post);
        this.emitPosts();

      },
      error => console.error('PostService.addPost(): ', error),
      () => console.info('PostService.addPost(): complete')
    );
  }

  /***Delete a Dog (you monster!) : delete a post ***/

  public deletePost(post) {

    return this.httpClient.delete<any>(`${this.api}/posts/${post.id}.json`, post).subscribe( //suprime en base de donnée
      data => {
        let index = this.posts.findIndex(p => p.id == post.id); //cherche le poste
        this.posts.splice(index, 1); //supprime
        this.emitPosts();
      },
      error => console.error('PostService.deletePost():', error),
      () => console.info('PostService.deletePost(): Fini')
    );
  }

  /***Modify a dog : Modify a post ***/

  public updatePost(post) {
    return this.httpClient.put<any>(`${this.api}/posts/${post.id}.json`, post).subscribe(
      data => {
        let index = this.posts.findIndex(p => p.id === post.id) // cherche l'index du post
        this.posts[index] = post;
        this.emitPosts();
      },
      error => console.error('PostService.updatePost():', error),
      () => console.info('PostService.updatePost():')
    );

  }



  /***Rate the dogs : methods for the likes buttons***/

  private _findById(id) {
    return this.posts.find((post) => post.id == id);
  }

  like(id) {
    let post = this._findById(id);
    post.likes += 1;
    let patch = { likes : post.likes};
    return this.httpClient.patch<any>(`${this.api}/posts/${post.id}.json`, patch).subscribe(
      data =>{
        this.emitPosts();
      },
      error => console.error('PostService.updatePost():', error),
      () => console.info('PostService.updatePost():')
    )
  }

}
