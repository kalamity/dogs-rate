import { Component, OnInit } from '@angular/core';
import {PostService} from '../services/post.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NgForm} from '@angular/forms';


@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  private id: string;
  private post: {};

  constructor(private postService: PostService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.post = this.postService.posts.find(post => post.id == this.id);
  }

  onSubmit(form: NgForm) {
    let post = form.value
    post.date = new Date()
    this.postService.updatePost(post)
    this.router.navigate(['/dogs'])
  }
}
